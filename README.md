# Flip Nullifier

- Notify you when something down
    - Расскажет, когда что-то упадет
- Unflip your service when you sleep
    - Восстановит упавшие сервисы, даже если вы спите и не видите уведомлений >_<
- And turtle is just cute :3
    - Да и вообще черепашки милые :3

![Flip Nulilfier Logo](https://cdn.discordapp.com/app-icons/545898205791911956/637f1bfcca87a7d19f7555f39e3e0df3.png)



### Prerequisites:
#### Docker: 


Mac OS:
```
brew install docker
```
Linux (Ubuntu):
```
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

### Installing

Пошаговая инструкция как заставить черепашку двигаться и смотреть по сторонам

Скачиваем образ:

```
docker pull username/dockerimage:tag
```

Запускаем: 

```
docker run --name turtle -v /some/content:/content/inside/container:ro - 80:80 -p 5000:5000 -d username/dockerimage:tag
```

Веб интерфейс доступен по адреcу localhost:80, 
Aпи доступно по адресу localhost:5000


## Deployment

Пример compose файла для докера:
```
version: '2'
services:
  turtle:
    image: "username/dockerimage:tag"
    ports:
      - "80:80"
      - "5000:5000"
    volumes:
      - ${PWD}/some/content:/content/inside/container
```

## Built With

* [Docker](https://www.docker.com/) - Enterprise Container Platform for High-Velocity Innovation
* [Maven](https://maven.apache.org/) - Dependency Management
* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Egor Lushnikov** - *Initial work* - [0xSauel](https://github.com/0xSauel)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
